﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphMLConnector
{
    public class clsEdgeItem
    {
        public clsNodeItem NodeItem1 { get; set; }
        public clsNodeItem NodeItem2 { get; set; }
        public clsOntologyItem OItem_RelationType { get; set; }
        public int SVGSourceId { get; set; }
        public clsOntologyItem XmlTemplate { get; set; } 
    }
}
