﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ontology_Module;
using System.IO;
using System.Web;
using System.IO;
using System.Reflection;
using ClassLibrary_ShellWork;
using OntologyClasses.BaseClasses;
using OntologyAppDBConnector;
using OntoMsg_Module;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;

namespace GraphMLConnector
{
    public class clsGraphMLWork
    {
        private clsLocalConfig objLocalConfig;
        private clsXMLTemplateWork objXMLTemplateWork;

        public List<clsClassRel> OList_ClassRel { get; set; }
        public List<clsClassAtt> OList_ClassAtt { get; set; }
        public List<clsOntologyItem> OList_ExportItems { get; set; }
        public List<clsOntologyItem> OList_Classes { get; set; }
        public List<clsOntologyItem> OList_ClassesWithChildren { get; set; }
        public List<clsOntologyItem> OList_Objects { get; set; }
        public List<clsOntologyItem> OList_AttributeTypes { get; set; }
        public List<clsOntologyItem> OList_RelationTypes { get; set; }
        public List<clsObjectRel> OList_ORels { get; set; }
        public List<clsObjectAtt> OList_OAtts { get; set; }

        public List<clsExportModes> OList_EModes { get; set; }

        private OntologyModDBConnector objDBLevel1;
        private OntologyModDBConnector objDBLevel2;

        private clsShellWork objShellWork = new clsShellWork();

        private List<clsNodeColor> nodeFillColors;
        private List<clsNodeColor> nodeNameColors;
        public List<clsSearchAndReplace> SearchAndReplaces;


        public void AddNodeColor(string GUID, string rgbColor)
        {
            if (!nodeFillColors.Any(nc => nc.GUID_Node == GUID))
            {
                nodeFillColors.Add(new clsNodeColor{GUID_Node = GUID, RGBColor = rgbColor});
            }
            else
            {
               nodeFillColors.Where(nc => nc.GUID_Node == GUID).ToList().ForEach(nc => nc.RGBColor = rgbColor); 
            }
        }

        public void RemoveNodeColor(string GUID)
        {
            nodeFillColors.RemoveAll(nc => nc.GUID_Node == GUID);
        }


        public clsGraphMLWork(clsLocalConfig localConfig)
        {
            objLocalConfig = localConfig;

            set_DBConnection();
            initialize();
        }

        public clsGraphMLWork(Globals Globals)
        {
            objLocalConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (objLocalConfig == null)
            {
                objLocalConfig = new clsLocalConfig(Globals);
                LocalConfigManager.AddLocalConfig(objLocalConfig);
            }

            set_DBConnection();
            initialize();
        }

        public void ClearLists()
        {
            OList_AttributeTypes.Clear();
            OList_ClassAtt.Clear();
            OList_ClassRel.Clear();
            OList_Classes.Clear();
            OList_ClassesWithChildren.Clear();
            OList_EModes.Clear();
            OList_ExportItems.Clear();
            OList_OAtts.Clear();
            OList_ORels.Clear();
            OList_Objects.Clear();
            OList_RelationTypes.Clear();
            nodeFillColors.Clear();
            nodeNameColors.Clear();
            SearchAndReplaces.Clear();
        }


        public clsOntologyItem GetItemLists(List<clsOntologyItemsOfOntologies> OList_OntologyItems,
            bool doClasses,
            bool doRelationTypes,
            bool doAttributeTypes,
            bool doObjects,
            bool doClassAtts,
            bool doClassRels,
            bool doObjectAtts,
            bool doObjectRels)
        {
            if (doAttributeTypes)
            {
                OList_AttributeTypes = OList_OntologyItems.Where(p => p.Type_Ref == objLocalConfig.Globals.Type_AttributeType).Select(p => new clsOntologyItem
                {
                    GUID = p.ID_Ref,
                    Name = p.Name_Ref,
                    GUID_Parent = p.ID_Parent_Ref,
                    Type = objLocalConfig.Globals.Type_AttributeType
                }).ToList();    
            }
            else
            {
                OList_AttributeTypes = new List<clsOntologyItem>();
            }

            if (doRelationTypes)
            {
                OList_RelationTypes = OList_OntologyItems.Where(p => p.Type_Ref == objLocalConfig.Globals.Type_RelationType).Select(p => new clsOntologyItem
                {
                    GUID = p.ID_Ref,
                    Name = p.Name_Ref,
                    GUID_Parent = p.ID_Parent_Ref,
                    Type = objLocalConfig.Globals.Type_RelationType
                }).ToList();    
            }
            else
            {
                OList_RelationTypes = new List<clsOntologyItem>();
            }


            if (doClasses)
            {
                OList_Classes = OList_OntologyItems.Where(p => p.Type_Ref == objLocalConfig.Globals.Type_Class).Select(p => new clsOntologyItem
                {
                    GUID = p.ID_Ref,
                    Name = p.Name_Ref,
                    GUID_Parent = p.ID_Parent_Ref,
                    Type = objLocalConfig.Globals.Type_Class
                }).ToList();    
            }
            else
            {
                OList_Classes = new List<clsOntologyItem>();   
            }


            if (doObjects)
            {
                OList_Objects = OList_OntologyItems.Where(p => p.Type_Ref == objLocalConfig.Globals.Type_Object).Select(p => new clsOntologyItem
                {
                    GUID = p.ID_Ref,
                    Name = p.Name_Ref,
                    GUID_Parent = p.ID_Parent_Ref,
                    Type = objLocalConfig.Globals.Type_Object
                }).ToList();    
            }
            else
            {
                OList_Objects = new List<clsOntologyItem>();
            }

            if (doClasses)
            {
                OList_ClassesWithChildren = (from objClass in OList_Classes
                                             join objOItem in OList_OntologyItems on objClass.GUID equals objOItem.ID_Ref
                                             where objOItem.ID_OntologyRelationRule == objLocalConfig.OItem_object_child_token.GUID
                                             select new clsOntologyItem
                                             {
                                                 GUID_Parent = objClass.GUID,
                                                 Type = objLocalConfig.Globals.Type_Object
                                             }).ToList();    
            }
            
            



            return GetRelatedItems(doClasses,
                doClassAtts,
                doClassRels,
                doObjectAtts,
                doObjectRels);

        }

        public clsOntologyItem GetRelatedItems(bool doClasses,
            bool doClassAtts,
            bool doClassRels,
            bool doObjectAtts,
            bool doObjectRels)
        {
            var objOItem_Result = objDBLevel1.GetDataClasses();
            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                if (doClasses)
                {
                    var oList_ClassesOfObjects = (from objClass in objDBLevel1.Classes1
                    join objObject in OList_Objects on objClass.GUID equals objObject.GUID_Parent
                    group objClass by objClass.GUID
                    into g
                    select new {GUID = g.Key, Classes = g}).ToList();

                    OList_Classes = OList_Classes.Concat((from objClass in oList_ClassesOfObjects
                        join objClassExist in OList_Classes on objClass.GUID equals
                            objClassExist.GUID into objClassesExist
                        from objClassExist in objClassesExist.DefaultIfEmpty()
                        where objClassExist == null
                        select
                            new clsOntologyItem(GUID_Item: objClass.GUID,
                                Name_Item: objClass.Classes.First().Name,
                                GUID_Item_Parent: objClass.Classes.First().GUID_Parent,
                                Type: objLocalConfig.Globals.Type_Class))
                        .ToList()).ToList();
                }
                

            //var oList_ClassesWithChildren = (from objClass in OList_Classes
                //                                 join objExportMode in OList_EModes on objClass.GUID equals
                //                                     objExportMode.ID_Item
                //                                 where
                //                                     objExportMode.ID_ExportMode ==
                //                                     objLocalConfig.OItem_object_grant_children_of_item.GUID
                //                                 select
                //                                     new clsOntologyItem
                //                                     {
                //                                         GUID = objClass.GUID,
                //                                         Type = objLocalConfig.Globals.Type_Object
                //                                     }).ToList();

               
                objOItem_Result = objLocalConfig.Globals.LState_Success;
                if (OList_ClassesWithChildren.Any())
                {
                    if (doClasses)
                    {
                        objOItem_Result = objDBLevel1.GetDataObjects(OList_ClassesWithChildren);
                        if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                        {
                            OList_Objects = OList_Objects.Concat(objDBLevel1.Objects1).ToList();
                        }    
                    }
                    


                }

                if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                {
                    
                    objOItem_Result = objDBLevel1.GetDataClassRel(null, true);

                    if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                    {
                        if (doClassRels)
                        {
                            OList_ClassRel = (from objClass_Left in OList_Classes
                                              join objClassRel in objDBLevel1.ClassRelsId on objClass_Left.GUID equals
                                                  objClassRel.ID_Class_Left
                                              join objClass_Right in OList_Classes on objClassRel.ID_Class_Right equals
                                                   objClass_Right.GUID
                                              join objRelType in OList_RelationTypes on objClassRel.ID_RelationType equals
                                                  objRelType.GUID
                                              select new clsClassRel()
                                              {
                                                  ID_Class_Left = objClassRel.ID_Class_Left,
                                                  ID_Class_Right = objClassRel.ID_Class_Right,
                                                  ID_RelationType = objClassRel.ID_RelationType,
                                                  Name_RelationType = objRelType.Name,
                                                  Min_Forw = objClassRel.Min_Forw,
                                                  Max_Forw = objClassRel.Max_Forw,
                                                  Max_Backw = objClassRel.Max_Backw,
                                                  Name_Class_Left = objClass_Left.Name,
                                                  Name_Class_Right = objClass_Right.Name
                                              }).ToList();    
                        }
                        


                        var oList_ClassAtt = new List<clsClassAtt>();
                        oList_ClassAtt.Add(new clsClassAtt(null, null, null, null, null));


                        objOItem_Result = objDBLevel1.GetDataClassAtts(null, null, false, true);
                        if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                        {
                            objOItem_Result = objDBLevel2.GetDataDataTypes(null, false);
                            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                            {
                                if (doClassAtts)
                                {
                                    OList_ClassAtt = (from objClass in OList_Classes
                                                      join objClassAtt in objDBLevel1.ClassAttsId on objClass.GUID equals
                                                          objClassAtt.ID_Class
                                                      join objAttType in OList_AttributeTypes on objClassAtt.ID_AttributeType equals
                                                          objAttType.GUID
                                                      join objDataType in objDBLevel2.DataTypes on objAttType.GUID_Parent equals objDataType.GUID
                                                      select new clsClassAtt()
                                                      {
                                                          ID_AttributeType = objAttType.GUID,
                                                          Name_AttributeType = objAttType.Name,
                                                          Name_DataType = objDataType.Name,
                                                          ID_Class = objClass.GUID,
                                                          ID_DataType = objDataType.GUID,
                                                          Min = objClassAtt.Min,
                                                          Max = objClassAtt.Max
                                                      }).ToList();    
                                }
                                


                                if (OList_Objects.Any() && doObjectRels)
                                {
                                    var searchObjectrels = (from obj1 in OList_Objects
                                        from rel in OList_RelationTypes
                                        from obj2 in OList_Objects
                                        select new clsObjectRel
                                        {
                                            ID_Object = obj1.GUID,
                                            ID_RelationType = rel.GUID,
                                            ID_Other = obj2.GUID
                                        }).ToList();

                                    if (searchObjectrels.Any())
                                    {
                                        objOItem_Result = objDBLevel1.GetDataObjectRel(searchObjectrels);    
                                    }
                                    

                                }
                                else
                                {
                                    objOItem_Result = objLocalConfig.Globals.LState_Nothing;
                                }

                                if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                                {
                                    if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                                    {
                                        
                                        OList_ORels = objDBLevel1.ObjectRels.Select(orel => orel.Clone() ).ToList();
                                    }
                                    else
                                    {
                                        OList_ORels.Clear();
                                    }


                                    if (OList_AttributeTypes.Any() && doObjectAtts)
                                    {
                                        var searchObjAtts = (from obj in OList_Objects
                                            from attType in OList_AttributeTypes
                                            select new clsObjectAtt
                                            {
                                                ID_Object = obj.GUID,
                                                ID_AttributeType = attType.GUID
                                            }).ToList();
                                        if (searchObjAtts.Any())
                                        {
                                            objOItem_Result = objDBLevel1.GetDataObjectAtt(searchObjAtts);    
                                        }
                                        
                                        if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                                        {
                                            OList_OAtts = objDBLevel1.ObjAtts.Select(objAtt => objAtt.Clone()).ToList();
                                        }
                                    }
                                    else
                                    {
                                        OList_OAtts.Clear();
                                    }


                                }
                                else if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Nothing.GUID)
                                {
                                    objOItem_Result = objLocalConfig.Globals.LState_Success;
                                }
                            }

                        }
                    }
                }

            }

            return objOItem_Result;
        }


        public clsOntologyItem ExportItems(string path, 
            bool doClasses = true, 
            bool doClassRel = true, 
            bool doClassAtt = true,
            bool doObjects = true, 
            bool doObjAtt = true, 
            bool doObjRel = true,
            bool doRelationTypes = true,
            bool doAttributeTypes = true)
        {
            var objOItem_Result = new clsOntologyItem();
            string NodeXML;
            string EdgeXML;

            try
            {
                if (!Directory.Exists(path.Substring(0,path.Length - Path.GetFileName(path).Length)))
                {
                    Directory.CreateDirectory(path.Substring(0,path.Length - Path.GetFileName(path).Length));
                }

                if (File.Exists(path))
                {
                    File.Delete(path);
                }

                var NodeXMLTempl = objXMLTemplateWork.UML_ClassNode;
                SearchAndReplaces.ForEach(sr => NodeXMLTempl = sr.ReplaceString(NodeXMLTempl));
                var EdgeXMLTempl = objXMLTemplateWork.UML_Edge;
                SearchAndReplaces.ForEach(sr => EdgeXMLTempl = sr.ReplaceString(EdgeXMLTempl));
            
                TextWriter objTextWriter = new StreamWriter(path);
                objTextWriter.WriteLine(objXMLTemplateWork.UML_Container.Substring(0, objXMLTemplateWork.UML_Container.IndexOf("@" + objLocalConfig.OItem_object_node_list.Name + "@") - 1));

                if (doClasses)
                {
                    
                    foreach (var oItem_Class in OList_Classes)
                    {
                        var nodeFillColorsTmp = nodeFillColors.Where(nc => nc.GUID_Node == oItem_Class.GUID).ToList();
                        var nodeNameColorsTmp = nodeNameColors.Where(nc => nc.GUID_Node == oItem_Class.GUID).ToList();


                        NodeXML = NodeXMLTempl;
                        NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_id.Name + "@", oItem_Class.GUID);
                        NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_name_node.Name + "@", HttpUtility.HtmlEncode(oItem_Class.Name));
                        if (!nodeFillColorsTmp.Any())
                        {
                            NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_color_fill.Name + "@", "#003300");    
                        }
                        else
                        {
                            NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_color_fill.Name + "@", "#" + nodeFillColorsTmp.First().RGBColor);    
                        }
                        
                        if (!nodeNameColorsTmp.Any())
                        {
                            NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_color_text.Name + "@", "#ffffff");    
                        }
                        else
                        {
                            NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_color_text.Name + "@", "#" + nodeNameColorsTmp.First().RGBColor);
                        }
                        

                        var ClassAtt = "";
                        var OList_ClassAtts = from objOClass in OList_ClassAtt
                                              where objOClass.ID_Class == oItem_Class.GUID
                                              select new { Caption = objOClass.Name_AttributeType + ": " + objOClass.Name_DataType };

                        if (doClassAtt)
                        {
                            foreach (var oItem_ClassAtt in OList_ClassAtts)
                            {
                                if (ClassAtt != "")
                                {
                                    ClassAtt += "\n";
                                }
                                ClassAtt += HttpUtility.HtmlEncode(oItem_ClassAtt.Caption);
                            }
                            NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_attrib_list.Name + "@", ClassAtt);
                        }
                        else
                        {
                            NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_attrib_list.Name + "@", "");
                        }
                        

                        objTextWriter.WriteLine(NodeXML);
                    }
                }

                NodeXMLTempl = objXMLTemplateWork.UML_ClassNode;
                SearchAndReplaces.ForEach(sr => NodeXMLTempl = sr.ReplaceString(NodeXMLTempl));
                EdgeXMLTempl = objXMLTemplateWork.UML_Edge;
                SearchAndReplaces.ForEach(sr => EdgeXMLTempl = sr.ReplaceString(EdgeXMLTempl));

                if (doClasses && doClassRel)
                {
                    
                    foreach (var oItem_ClassRel in OList_ClassRel)
                    {
                        EdgeXML = EdgeXMLTempl;
                        EdgeXML = EdgeXML.Replace("@" + objLocalConfig.OItem_object_id.Name + "@",
                                                  oItem_ClassRel.ID_Class_Left + oItem_ClassRel.ID_Class_Right +
                                                  oItem_ClassRel.ID_RelationType);
                        EdgeXML = EdgeXML.Replace("@" + objLocalConfig.OItem_object_id_left.Name + "@",
                                                  oItem_ClassRel.ID_Class_Left);
                        EdgeXML = EdgeXML.Replace("@" + objLocalConfig.OItem_object_id_right.Name + "@",
                                                  oItem_ClassRel.ID_Class_Right);
                        EdgeXML = EdgeXML.Replace("@" + objLocalConfig.OItem_object_name_relationtype.Name + "@",
                                                  HttpUtility.HtmlEncode(oItem_ClassRel.Name_RelationType));

                        objTextWriter.WriteLine(EdgeXML);
                    }
                }

                NodeXMLTempl = objXMLTemplateWork.UML_ClassNode;
                SearchAndReplaces.ForEach(sr => NodeXMLTempl = sr.ReplaceString(NodeXMLTempl));
                EdgeXMLTempl = objXMLTemplateWork.UML_Edge;
                SearchAndReplaces.ForEach(sr => EdgeXMLTempl = sr.ReplaceString(EdgeXMLTempl));

                if (doObjects)
                {
                    

                    

                    foreach (var oItem_Object in OList_Objects)
                    {
                        var nodeColorsTmp = nodeFillColors.Where(nc => nc.GUID_Node == oItem_Object.GUID).ToList();
                        var nodeNameColorsTmp = nodeNameColors.Where(nc => nc.GUID_Node == oItem_Object.GUID).ToList();

                        NodeXML = NodeXMLTempl;
                        NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_id.Name + "@", oItem_Object.GUID);
                        NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_name_node.Name + "@", HttpUtility.HtmlEncode(oItem_Object.Name));

                        if (!nodeColorsTmp.Any())
                        {
                            NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_color_fill.Name + "@", "#ffcc00");
                        }
                        else
                        {
                            NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_color_fill.Name + "@", "#" + nodeColorsTmp.First().RGBColor);
                        }

                        if (!nodeNameColorsTmp.Any())
                        {
                            NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_color_text.Name + "@", "#000000");
                        }
                        else
                        {
                            NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_color_text.Name + "@", "#" + nodeNameColorsTmp.First().RGBColor);
                        }

                        
                        

                        var ObjAtt = "";

                        var OList_ObjAtts = from objOAtt in OList_OAtts
                                            where objOAtt.ID_Object == oItem_Object.GUID
                                            select
                                                new
                                                {
                                                    Caption =
                                            objOAtt.Name_AttributeType + ": " +
                                            (objOAtt.Val_Named.Length > 20
                                                 ? objOAtt.Val_Named.Substring(0, 20)
                                                 : objOAtt.Val_Named)
                                                };
                        if (doObjAtt)
                        {
                            foreach (var objOAtt in OList_ObjAtts)
                            {
                                if (ObjAtt != "")
                                {
                                    ObjAtt += "\n";
                                }
                                ObjAtt += HttpUtility.HtmlEncode(objOAtt.Caption);
                            }
                            NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_attrib_list.Name + "@", ObjAtt);
                        }
                        else
                        {
                            NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_attrib_list.Name + "@", "");
                        }
                        

                        objTextWriter.WriteLine(NodeXML);

                        if (doClasses)
                        {
                            EdgeXML = EdgeXMLTempl;
                            EdgeXML = EdgeXML.Replace("@" + objLocalConfig.OItem_object_id.Name + "@",
                                                      oItem_Object.GUID + oItem_Object.GUID_Parent);
                            EdgeXML = EdgeXML.Replace("@" + objLocalConfig.OItem_object_id_left.Name + "@",
                                                      oItem_Object.GUID_Parent);
                            EdgeXML = EdgeXML.Replace("@" + objLocalConfig.OItem_object_id_right.Name + "@",
                                                      oItem_Object.GUID);
                            EdgeXML = EdgeXML.Replace("@" + objLocalConfig.OItem_object_name_relationtype.Name + "@",
                                                      HttpUtility.HtmlEncode("Instance"));

                            objTextWriter.WriteLine(EdgeXML);
                        }
                        
                    }
                }


                NodeXMLTempl = objXMLTemplateWork.UML_ClassNode;
                SearchAndReplaces.ForEach(sr => NodeXMLTempl = sr.ReplaceString(NodeXMLTempl));
                EdgeXMLTempl = objXMLTemplateWork.UML_Edge;
                SearchAndReplaces.ForEach(sr => EdgeXMLTempl = sr.ReplaceString(EdgeXMLTempl));

                if (doRelationTypes)
                {
                    var objRelNodes = (from objRelType in OList_RelationTypes
                                       join objORel in OList_ORels on objRelType.GUID equals objORel.ID_Other
                                       select objRelType).ToList();

                    foreach (var oItem_Rel in objRelNodes)
                    {
                        var nodeColorsTmp = nodeFillColors.Where(nc => nc.GUID_Node == oItem_Rel.GUID).ToList();
                        var nodeNameColorsTmp = nodeNameColors.Where(nc => nc.GUID_Node == oItem_Rel.GUID).ToList();

                        NodeXML = NodeXMLTempl;
                        NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_id.Name + "@", oItem_Rel.GUID);
                        NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_name_node.Name + "@", HttpUtility.HtmlEncode(oItem_Rel.Name));

                        if (!nodeColorsTmp.Any())
                        {
                            NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_color_fill.Name + "@", "#ffcc00");
                        }
                        else
                        {
                            NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_color_fill.Name + "@", "#" + nodeColorsTmp.First().RGBColor);
                        }

                        if (!nodeNameColorsTmp.Any())
                        {
                            NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_color_text.Name + "@", "#000000");
                        }
                        else
                        {
                            NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_color_text.Name + "@", "#" + nodeNameColorsTmp.First().RGBColor);
                        }
                        
                        
                        NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_attrib_list.Name + "@", "");
                        objTextWriter.WriteLine(NodeXML);
                    }
                }

                NodeXMLTempl = objXMLTemplateWork.UML_ClassNode;
                SearchAndReplaces.ForEach(sr => NodeXMLTempl = sr.ReplaceString(NodeXMLTempl));
                EdgeXMLTempl = objXMLTemplateWork.UML_Edge;
                SearchAndReplaces.ForEach(sr => EdgeXMLTempl = sr.ReplaceString(EdgeXMLTempl));

                if (doAttributeTypes)
                {
                    var objAttNodes = (from objAttType in OList_AttributeTypes
                                       join objORel in OList_ORels on objAttType.GUID equals objORel.ID_Other
                                       select objAttType).ToList();

                    foreach (var oItem_Rel in objAttNodes)
                    {
                        var nodeColorsTmp = nodeFillColors.Where(nc => nc.GUID_Node == oItem_Rel.GUID).ToList();
                        var nodeNameColorsTmp = nodeNameColors.Where(nc => nc.GUID_Node == oItem_Rel.GUID).ToList();

                        NodeXML =NodeXMLTempl;
                        NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_id.Name + "@", oItem_Rel.GUID);
                        NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_name_node.Name + "@", HttpUtility.HtmlEncode(oItem_Rel.Name));

                        if (!nodeColorsTmp.Any())
                        {
                            NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_color_fill.Name + "@", "#ffcc00");
                        }
                        else
                        {
                            NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_color_fill.Name + "@", "#" + nodeColorsTmp.First().RGBColor);
                        }

                        if (!nodeNameColorsTmp.Any())
                        {
                            NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_color_text.Name + "@", "#000000");
                        }
                        else
                        {
                            NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_color_text.Name + "@", "#" + nodeNameColorsTmp.First().RGBColor);
                        }
                        
                        
                        NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_attrib_list.Name + "@", "");
                        objTextWriter.WriteLine(NodeXML);
                    }
                }


                NodeXMLTempl = objXMLTemplateWork.UML_ClassNode;
                SearchAndReplaces.ForEach(sr => NodeXMLTempl = sr.ReplaceString(NodeXMLTempl));
                EdgeXMLTempl = objXMLTemplateWork.UML_Edge;
                SearchAndReplaces.ForEach(sr => EdgeXMLTempl = sr.ReplaceString(EdgeXMLTempl));

                if (doClasses && doObjRel)
                {
                    var objClassNodes = (from objClass in OList_Classes
                                         join objORel in OList_ORels on objClass.GUID equals objORel.ID_Other
                                         select objClass).ToList();

                    foreach (var oItem_Rel in objClassNodes)
                    {
                        var nodeColorsTmp = nodeFillColors.Where(nc => nc.GUID_Node == oItem_Rel.GUID).ToList();
                        var nodeNameColorsTmp = nodeNameColors.Where(nc => nc.GUID_Node == oItem_Rel.GUID).ToList();

                        NodeXML = NodeXMLTempl;
                        NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_id.Name + "@", oItem_Rel.GUID);
                        NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_name_node.Name + "@", HttpUtility.HtmlEncode(oItem_Rel.Name));

                        if (!nodeColorsTmp.Any())
                        {
                            NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_color_fill.Name + "@", "#ffcc00");
                        }
                        else
                        {
                            NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_color_fill.Name + "@", "#" + nodeColorsTmp.First().RGBColor);
                        }

                        if (!nodeNameColorsTmp.Any())
                        {
                            NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_color_text.Name + "@", "#000000");
                        }
                        else
                        {
                            NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_color_text.Name + "@", "#" + nodeNameColorsTmp.First().RGBColor);
                        }
                        
                        
                        NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_attrib_list.Name + "@", "");
                        objTextWriter.WriteLine(NodeXML);
                    }
                }

                NodeXMLTempl = objXMLTemplateWork.UML_ClassNode;
                SearchAndReplaces.ForEach(sr => NodeXMLTempl = sr.ReplaceString(NodeXMLTempl));
                EdgeXMLTempl = objXMLTemplateWork.UML_Edge;
                SearchAndReplaces.ForEach(sr => EdgeXMLTempl = sr.ReplaceString(EdgeXMLTempl));

                if (doObjRel)
                {
                    foreach (var oItem_ORel in OList_ORels)
                    {
                        EdgeXML = EdgeXMLTempl;
                        EdgeXML = EdgeXML.Replace("@" + objLocalConfig.OItem_object_id.Name + "@",
                                                  oItem_ORel.ID_Object + oItem_ORel.ID_Other +
                                                  oItem_ORel.ID_RelationType);
                        EdgeXML = EdgeXML.Replace("@" + objLocalConfig.OItem_object_id_left.Name + "@",
                                                  oItem_ORel.ID_Object);
                        EdgeXML = EdgeXML.Replace("@" + objLocalConfig.OItem_object_id_right.Name + "@",
                                                  oItem_ORel.ID_Other);
                        EdgeXML = EdgeXML.Replace("@" + objLocalConfig.OItem_object_name_relationtype.Name + "@",
                                                  HttpUtility.HtmlEncode(oItem_ORel.Name_RelationType));

                        objTextWriter.WriteLine(EdgeXML);
                    }
                }






                objTextWriter.WriteLine(objXMLTemplateWork.UML_Container.Substring(objXMLTemplateWork.UML_Container.IndexOf("@" + objLocalConfig.OItem_object_edge_list.Name + "@") + ("@" + objLocalConfig.OItem_object_edge_list.Name + "@").Length).Replace("@" + objLocalConfig.OItem_object_resource_list.Name + "@", ""));
                objTextWriter.Close();

                //if (File.Exists(path))
                //{
                //    if (
                //        !objShellWork.start_Process(path, "", path.Substring(0, path.Length - Path.GetFileName(path).Length),
                //                                    false, false))
                //    {
                //        objOItem_Result = objLocalConfig.Globals.LState_Error;
                //    }

                //}

                return objLocalConfig.Globals.LState_Success.Clone();
            }
            catch (Exception e)
            {
                return objLocalConfig.Globals.LState_Error.Clone();
            }
            
        }
        private void set_DBConnection()
        {
            objDBLevel1 = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel2 = new OntologyModDBConnector(objLocalConfig.Globals);
        }

        public clsOntologyItem ExportGraph(string Path, List<clsNodeItem> nodeList, List<clsEdgeItem> edgeList, List<clsGroupItem> groupItems, List<clsOntologyItem> svgSources)
        {

            clsOntologyItem objOItem_Result;
            
            using(TextWriter objTextWriter = new StreamWriter(Path))
            {
                string NodeXML;
                string EdgeXML;
                var nodeEdgeCollection = new clsNodeEdgeCollection();
                
                objTextWriter.WriteLine(objXMLTemplateWork.UML_Container.Substring(0, objXMLTemplateWork.UML_Container.IndexOf("@" + objLocalConfig.OItem_object_node_list.Name + "@") - 1));

                if (groupItems.Any())
                {

                    groupItems.ForEach(gi =>
                        {
                            nodeEdgeCollection = CreateGroups(gi, objTextWriter);
                        });
                    
                }
                (from node in nodeList
                 join nodeDone in nodeEdgeCollection.NodeItems on node equals nodeDone into nodesDone1
                 from nodeDone in nodesDone1.DefaultIfEmpty()
                 where nodeDone == null
                 select node).ToList().ForEach(nd =>
                    {
                        if (nd.XmlTemplate != null)
                        {
                            NodeXML = objXMLTemplateWork.GetXMLContent(nd.XmlTemplate);
                        }
                        else
                        {
                            NodeXML = objXMLTemplateWork.UML_ClassNode;
                        }
                        
                        NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_id.Name + "@", nd.IdNode);
                        NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_name_node.Name + "@", HttpUtility.HtmlEncode(nd.NameNode));
                        NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_color_fill.Name + "@", "#00ff00");
                        NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_color_text.Name + "@", "#ffffff");
                        NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_source_id.Name + "@", nd.SVGSourceId.ToString());

                        nd.VariableValueMapList.ForEach(vvm =>
                            {
                                NodeXML = NodeXML.Replace("@" + vvm.Variable + "@", HttpUtility.HtmlEncode(vvm.Value));
                            });

                        NodeXML = GetSubNodeText(nd, NodeXML);

                        objTextWriter.WriteLine(NodeXML);
                    });

                (from edge in edgeList
                 join edgeDone in nodeEdgeCollection.EdgeItems on edge equals edgeDone into edgesDone1
                 from edgeDone in edgesDone1.DefaultIfEmpty()
                 where edgeDone == null
                 select edge).ToList().ForEach(edg =>
                    {
                        if (edg.XmlTemplate != null)
                        {
                            EdgeXML = objXMLTemplateWork.GetXMLContent(edg.XmlTemplate);
                        }
                        else
                        {
                            EdgeXML = objXMLTemplateWork.UML_Edge;
                        }
                        EdgeXML = EdgeXML.Replace("@" + objLocalConfig.OItem_object_id.Name + "@",
                                                  edg.NodeItem1.IdNode + edg.NodeItem2.IdNode +
                                                  edg.OItem_RelationType.GUID);
                        EdgeXML = EdgeXML.Replace("@" + objLocalConfig.OItem_object_id_left.Name + "@",
                                                  edg.NodeItem1.IdNode);
                        EdgeXML = EdgeXML.Replace("@" + objLocalConfig.OItem_object_id_right.Name + "@",
                                                  edg.NodeItem2.IdNode);
                        EdgeXML = EdgeXML.Replace("@" + objLocalConfig.OItem_object_name_relationtype.Name + "@",
                                                  HttpUtility.HtmlEncode(edg.OItem_RelationType.Name));
                        EdgeXML = EdgeXML.Replace("@" + objLocalConfig.OItem_object_source_id.Name + "@", edg.SVGSourceId.ToString());

                        objTextWriter.WriteLine(EdgeXML);
                    });

                var containerRest = objXMLTemplateWork.UML_Container.Substring(objXMLTemplateWork.UML_Container.IndexOf("@" + objLocalConfig.OItem_object_edge_list.Name + "@") + ("@" + objLocalConfig.OItem_object_edge_list.Name + "@").Length);
                var toResources = containerRest.Substring(0, containerRest.IndexOf("@" + objLocalConfig.OItem_object_resource_list.Name + "@")-1);

                objTextWriter.WriteLine(toResources);

                for (int i = 0; i < svgSources.Count; i++)
                {
                    var resourceString = objXMLTemplateWork.GetXMLContent(svgSources[i]);
                    resourceString = resourceString.Replace("@" + objLocalConfig.OItem_object_source_id.Name + "@", (i + 1).ToString());

                    objTextWriter.WriteLine(resourceString);
                }

                containerRest = containerRest.Substring(toResources.Length + ("@" + objLocalConfig.OItem_object_resource_list.Name + "@").Length + 1);

                objTextWriter.WriteLine(containerRest);
                
            }


            return objLocalConfig.Globals.LState_Success.Clone();
            
        }

        private string GetSubNodeText(clsNodeItem parentItem, string nodeText)
        {
            
            parentItem.SubNodeList.ForEach(snl =>
                {
                    var textSubNodes = "";        
                    snl.SubNodes.ForEach(sn =>
                        {
                            var textSubNode = "";
                            if (sn.XmlTemplate != null)
                            {
                                textSubNode = objXMLTemplateWork.GetXMLContent(sn.XmlTemplate);
                            }
                            if (!sn.VariableValueMapList.Any())
                            {
                                if (!string.IsNullOrEmpty(textSubNode))
                                {
                                    textSubNode += "\n";
                                }
                                textSubNode += sn.NameNode;
                            }
                            else
                            {
                                sn.VariableValueMapList.ForEach(vvm =>
                                    {
                                        textSubNode = textSubNode.Replace("@" + vvm.Value + "@", HttpUtility.HtmlEncode(vvm.Value));
                                    });
                            }
                            if (!string.IsNullOrEmpty(textSubNodes))
                            {
                                textSubNodes += "\n";
                            }
                            textSubNodes += textSubNode;
                            textSubNodes = GetSubNodeText(sn, textSubNodes);
                        });

                    nodeText = nodeText.Replace("@" + snl.Variable + "@", textSubNodes);
                });

            return nodeText;
        }

        private clsNodeEdgeCollection CreateGroups(clsGroupItem groupItemParent, TextWriter objTextWriter)
        {
            var result = new clsNodeEdgeCollection();
            string nodeXML;
            string edgeXML;
            var groupString = objXMLTemplateWork.UML_Group.Replace("@" + objLocalConfig.OItem_object_id.Name + "@", groupItemParent.GroupId)
                                    .Replace("@" + objLocalConfig.OItem_object_graph_id.Name + "@", groupItemParent.GraphId)
                                    .Replace("@" + objLocalConfig.OItem_object_name_node.Name + "@", groupItemParent.GroupName);
            objTextWriter.WriteLine(groupString.Substring(0, groupString.IndexOf("@" + objLocalConfig.OItem_object_graph_items.Name + "@") - 1));

            result.NodeItems = groupItemParent.Nodes;
            result.EdgeItems = groupItemParent.Edges;

            groupItemParent.Nodes.ForEach(nd =>
            {
                if (nd.XmlTemplate != null)
                {
                    nodeXML = objXMLTemplateWork.GetXMLContent(nd.XmlTemplate);
                }
                else
                {
                    nodeXML = objXMLTemplateWork.UML_ClassNode;
                }

                nodeXML = nodeXML.Replace("@" + objLocalConfig.OItem_object_id.Name + "@", nd.IdNode);
                nodeXML = nodeXML.Replace("@" + objLocalConfig.OItem_object_name_node.Name + "@", HttpUtility.HtmlEncode(nd.NameNode));
                nodeXML = nodeXML.Replace("@" + objLocalConfig.OItem_object_color_fill.Name + "@", "#00ff00");
                nodeXML = nodeXML.Replace("@" + objLocalConfig.OItem_object_color_text.Name + "@", "#ffffff");
                nodeXML = nodeXML.Replace("@" + objLocalConfig.OItem_object_source_id.Name + "@", nd.SVGSourceId.ToString());

                nd.VariableValueMapList.ForEach(vvm =>
                {
                    nodeXML = nodeXML.Replace("@" + vvm.Variable + "@", HttpUtility.HtmlEncode(vvm.Value));
                });

                nodeXML = GetSubNodeText(nd, nodeXML);

                objTextWriter.WriteLine(nodeXML);
            });

            groupItemParent.Edges.ForEach(edg =>
            {
                if (edg.XmlTemplate != null)
                {
                    edgeXML = objXMLTemplateWork.GetXMLContent(edg.XmlTemplate);
                }
                else
                {
                    edgeXML = objXMLTemplateWork.UML_ClassNode;
                }
                edgeXML = edgeXML.Replace("@" + objLocalConfig.OItem_object_id.Name + "@",
                                          edg.NodeItem1.IdNode + edg.NodeItem2.IdNode +
                                          edg.OItem_RelationType.GUID);
                edgeXML = edgeXML.Replace("@" + objLocalConfig.OItem_object_id_left.Name + "@",
                                          edg.NodeItem1.IdNode);
                edgeXML = edgeXML.Replace("@" + objLocalConfig.OItem_object_id_right.Name + "@",
                                          edg.NodeItem2.IdNode);
                edgeXML = edgeXML.Replace("@" + objLocalConfig.OItem_object_name_relationtype.Name + "@",
                                          HttpUtility.HtmlEncode(edg.OItem_RelationType.Name));
                edgeXML = edgeXML.Replace("@" + objLocalConfig.OItem_object_source_id.Name + "@", edg.SVGSourceId.ToString());

                objTextWriter.WriteLine(edgeXML);
            });

            groupItemParent.GroupItems.ForEach(gi =>
                {
                    var addNodeEdges = CreateGroups(gi, objTextWriter);
                    result.NodeItems.AddRange(addNodeEdges.NodeItems);
                    result.EdgeItems.AddRange(addNodeEdges.EdgeItems);   
                });

            objTextWriter.WriteLine(groupString.Substring(groupString.IndexOf("@" + objLocalConfig.OItem_object_graph_items.Name + "@") + ("@" + objLocalConfig.OItem_object_graph_items.Name + "@").Length));

            return result;
        }

        public clsOntologyItem ExportClasses(bool doClasses, bool doObjects, bool doClassRel, bool doObjectRels, string Path)
        {
            clsOntologyItem objOItem_Result;
            TextWriter objTextWriter = new StreamWriter(Path);
            string NodeXML;
            string EdgeXML;

            objOItem_Result = objDBLevel1.GetDataClassRel(doIds:false,classRels:null);

            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                objOItem_Result = objDBLevel2.GetDataClasses();
                if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                {
                    var NodeXMLTempl = objXMLTemplateWork.UML_ClassNode;

                    SearchAndReplaces.ForEach(sr => NodeXMLTempl = sr.ReplaceString(NodeXMLTempl));

                    objTextWriter.WriteLine(objXMLTemplateWork.UML_Container.Substring(0,objXMLTemplateWork.UML_Container.IndexOf("@" + objLocalConfig.OItem_object_node_list.Name + "@")-1));
                    foreach (var objClass in objDBLevel2.Classes1)
                    {
                        NodeXML = NodeXMLTempl;
                        NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_id.Name + "@", objClass.GUID);
                        NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_name_node.Name + "@", HttpUtility.HtmlEncode(objClass.Name));
                        NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_attrib_list.Name + "@", "");
                        NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_color_fill.Name + "@", "#00ff00");
                        NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_color_text.Name + "@", "#ffffff");
                        objTextWriter.WriteLine(NodeXML);

                    }

                    if (doClassRel)
                    {
                        foreach (var objClassRel in objDBLevel1.ClassRels)
                        {
                            EdgeXML = objXMLTemplateWork.UML_Edge;
                            EdgeXML = EdgeXML.Replace("@" + objLocalConfig.OItem_object_id.Name + "@",
                                                      objClassRel.ID_Class_Left + objClassRel.ID_Class_Right +
                                                      objClassRel.ID_RelationType);
                            EdgeXML = EdgeXML.Replace("@" + objLocalConfig.OItem_object_id_left.Name + "@",
                                                      objClassRel.ID_Class_Left);
                            EdgeXML = EdgeXML.Replace("@" + objLocalConfig.OItem_object_id_right.Name + "@",
                                                      objClassRel.ID_Class_Right);
                            EdgeXML = EdgeXML.Replace("@" + objLocalConfig.OItem_object_name_relationtype.Name + "@",
                                                      HttpUtility.HtmlEncode(objClassRel.Name_RelationType));

                            objTextWriter.WriteLine(EdgeXML);
                        }    
                    }
                    
                }

                if (doObjects)
                {
                    objOItem_Result = objDBLevel2.GetDataObjects();
                    if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                    {
                        foreach (var objObject in objDBLevel2.Objects1)
                        {
                            NodeXML = objXMLTemplateWork.UML_ClassNode;
                            NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_id.Name + "@", objObject.GUID);
                            NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_name_node.Name + "@", HttpUtility.HtmlEncode(objObject.Name));
                            NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_attrib_list.Name + "@", "");
                            NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_color_fill.Name + "@", "#ffcc00");
                            NodeXML = NodeXML.Replace("@" + objLocalConfig.OItem_object_color_text.Name + "@", "#000000");
                            objTextWriter.WriteLine(NodeXML);

                        }
                    }
                    
                   

                    
                }


                if (doObjectRels)
                {
                    objOItem_Result = objDBLevel1.GetDataObjectRel(null,false,false);
                    if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                    {
                        foreach (var objObjRel in objDBLevel1.ObjectRels)
                        {
                            EdgeXML = objXMLTemplateWork.UML_Edge;
                            EdgeXML = EdgeXML.Replace("@" + objLocalConfig.OItem_object_id.Name + "@",
                                                      objObjRel.ID_Object + objObjRel.ID_Other +
                                                      objObjRel.ID_RelationType);
                            EdgeXML = EdgeXML.Replace("@" + objLocalConfig.OItem_object_id_left.Name + "@",
                                                      objObjRel.ID_Object);
                            EdgeXML = EdgeXML.Replace("@" + objLocalConfig.OItem_object_id_right.Name + "@",
                                                      objObjRel.ID_Other);
                            EdgeXML = EdgeXML.Replace("@" + objLocalConfig.OItem_object_name_relationtype.Name + "@",
                                                      objObjRel.Name_RelationType);

                            objTextWriter.WriteLine(EdgeXML);
                        }    
                    }
                }
                
            }
            objTextWriter.WriteLine(objXMLTemplateWork.UML_Container.Substring(objXMLTemplateWork.UML_Container.IndexOf("@" + objLocalConfig.OItem_object_edge_list.Name + "@") + ("@" + objLocalConfig.OItem_object_edge_list.Name + "@").Length).Replace("@" + objLocalConfig.OItem_object_resource_list.Name + "@",""));
            objTextWriter.Close();
            return objOItem_Result;
        }

        private void initialize()
        {
            objXMLTemplateWork = new clsXMLTemplateWork(objLocalConfig);
            OList_AttributeTypes = new List<clsOntologyItem>();
            OList_ClassAtt = new List<clsClassAtt>();
            OList_ClassRel = new List<clsClassRel>();
            OList_ExportItems = new List<clsOntologyItem>();
            OList_OAtts = new List<clsObjectAtt>();
            OList_ORels = new List<clsObjectRel>();
            OList_EModes = new List<clsExportModes>();
            OList_Classes = new List<clsOntologyItem>();
            OList_ClassesWithChildren = new List<clsOntologyItem>();
            OList_EModes = new List<clsExportModes>();
            OList_Objects = new List<clsOntologyItem>();
            OList_RelationTypes = new List<clsOntologyItem>();
            nodeFillColors = new List<clsNodeColor>();
            nodeNameColors = new List<clsNodeColor>();
            SearchAndReplaces = new List<clsSearchAndReplace>();
        }

    }
}
