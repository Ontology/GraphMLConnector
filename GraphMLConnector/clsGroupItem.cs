﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphMLConnector
{
    public class clsGroupItem
    {
        public string GroupId { get; set; }
        public string GroupName { get; set; }
        public string GraphId { get; set; }
        
        public List<clsNodeItem> Nodes { get; set; }
        public List<clsEdgeItem> Edges { get; set; }
        public List<clsGroupItem> GroupItems { get; set; }

        public clsGroupItem()
        {
            Nodes = new List<clsNodeItem>();
            Edges = new List<clsEdgeItem>();
            GroupItems = new List<clsGroupItem>();
        }
    }
}
