﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphMLConnector
{
    public class clsNodeEdgeCollection
    {
        public List<clsNodeItem> NodeItems { get; set; }
        public List<clsEdgeItem> EdgeItems { get; set; }

        public clsNodeEdgeCollection()
        {
            NodeItems = new List<clsNodeItem>();
            EdgeItems = new List<clsEdgeItem>();
        }
    }
}
