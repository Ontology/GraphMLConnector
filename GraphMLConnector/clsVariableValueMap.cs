﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphMLConnector
{
    public class clsVariableValueMap
    {
        public string Variable { get; set; }
        public string Value { get; set; }
    }
}
