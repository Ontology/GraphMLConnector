﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphMLConnector
{
    public class clsSubNodeList
    {
        public string Variable { get; set; }
        public List<clsNodeItem> SubNodes { get; set; }

        public clsSubNodeList()
        {
            SubNodes = new List<clsNodeItem>();
        }
    }
}
