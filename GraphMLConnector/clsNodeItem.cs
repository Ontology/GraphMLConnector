﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphMLConnector
{
    public class clsNodeItem
    {
        public string IdNode { get; set; }
        public string NameNode { get; set; }
        public int SVGSourceId { get; set; }
        public clsOntologyItem XmlTemplate { get; set; }
        public List<clsSubNodeList> SubNodeList { get; set; }
        public List<clsVariableValueMap> VariableValueMapList { get; set; }

        public clsNodeItem()
        {
            SubNodeList = new List<clsSubNodeList>();
            VariableValueMapList = new List<clsVariableValueMap>();
        }
    }
}
